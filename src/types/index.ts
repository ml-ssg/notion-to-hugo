import { GetPageResponse } from '@notionhq/client/build/src/api-endpoints';
import {
  ListBlockChildrenResponseResult,
  NotionToMarkdownOptions
} from 'notion-to-md/build/types';
import slugify from 'slugify';
export type NotionToHugoOptions = NotionToMarkdownOptions;

/**
 * The frontmatter is the piece of YAML (default), TOML or JSON code
 * that lies on top of Hugo Markdown files. It is used to configure
 * Hugo on a page basis.
 *
 * In Hugo the frontmatter format can be set in the configuration file.
 */
export type Frontmatter = {
  // Properties available to all Notion pages who declare 'properties'
  id: string;
  title: string;
  created_time: Date;
  last_edited_time: Date;
  archived: boolean;
  cover?: string;
  icon?: string;
  // Properties computed for our needs
  slug?: string;
  // Any other property is accepted
  [key: string]: any;
};

/**
 * Simplified Notion database page property type to ease property manipulation and to know what to expect.
 */
export type SimplePropertyValueType =
  | string
  | number
  | string[]
  | boolean
  | Date
  | undefined;

/**
 * A page is a Hugo Markdown file which represents a single unit of content.
 * It is used to render a full HTML file or a part of a HTML file for
 * collections for instance.
 */
export class Page {
  readonly notionPage: GetPageResponse;
  id: string;
  frontmatter?: Frontmatter;
  parent?: Page;
  children?: Page[];
  path?: string;
  parent_id?: string;
  language?: string;
  readonly properties?: { [id: string]: SimplePropertyValueType };

  /**
   * Creates a Hugo page from a Notion page
   * @param notionPage A page as returned by Notion API
   */
  constructor(notionPage: GetPageResponse) {
    this.notionPage = notionPage;
    this.id = notionPage.id.replace(/-/g, '');

    // We are dealing with a "full" page object
    if ('properties' in notionPage) {
      this.frontmatter = {
        id: notionPage.id.replace(/-/g, ''),
        created_time: new Date(notionPage.created_time),
        last_edited_time: new Date(notionPage.last_edited_time),
        archived: notionPage.archived,
        title: ''
      };

      // Notion page cover
      if (notionPage.cover) {
        if (notionPage.cover?.type === 'external') {
          this.frontmatter.cover = notionPage.cover.external.url;
        } else {
          this.frontmatter.cover = notionPage.cover.file.url;
        }
      }

      // Notion page icon
      if (notionPage.icon) {
        if (notionPage.icon.type == 'emoji') {
          this.frontmatter.icon = notionPage.icon.emoji;
        } else if (notionPage.icon.type === 'external') {
          this.frontmatter.icon = notionPage.icon.external.url;
        }
      }

      // Custom notionPage properties
      for (let [name, value] of Object.entries(notionPage.properties)) {
        if (value.type === 'title') {
          this.frontmatter.title = value.title
            .map((el) => el.plain_text)
            .join('')
            .trim();
          this.frontmatter.slug = slugify(this.frontmatter.title, {
            lower: true,
            strict: true
          });
        } else {
          if (name == 'Weight' && value.type == 'number' && value.number) {
            this.frontmatter.weight = value.number;
          }
        }
      }

      // Retrieves the pages' properties as a simple dict
      this.properties = this.simpleProperties();
      this.frontmatter = {
        ...this.frontmatter,
        ...this.properties
      };

      // Add usual Hugo frontmatter params from page properties
      if ('tags' in this.properties)
        this.frontmatter.tags = this.properties.tags;
    } else {
      // Notion returned a minimal page object.
      console.dir(notionPage.object);
      this.properties = {};
    }
  }

  assertUnreachable(_x: any): never {
    throw new Error('Unhandled property:' + <string>_x);
  }

  userToString(userBase: { id: string; name?: string | null }) {
    return `${userBase.id}: ${userBase.name || 'Unknown Name'}`;
  }

  simpleProperty(
    // @ts-ignore
    property: GetPageResponse['properties'][string]
  ): SimplePropertyValueType {
    switch (property.type) {
      case 'checkbox':
        return property.checkbox;
      case 'created_by':
        return this.userToString(property.created_by);
      case 'created_time':
        return new Date(property.created_time);
      case 'date':
        return property.date ? new Date(property.date.start) : undefined;
      case 'email':
        return property.email;
      case 'url':
        return property.url;
      case 'number':
        return property.number;
      case 'phone_number':
        return property.phone_number;
      case 'select':
        if (!property.select) {
          return undefined;
        }
        return property.select.name;
      case 'multi_select':
        if (!property.multi_select) {
          return undefined;
        }
        return property.multi_select.map((select: any) => select.name);
      case 'people':
        return property.people
          .map((person: any) => this.userToString(person))
          .join(', ');
      case 'last_edited_by':
        return this.userToString(property.last_edited_by);
      case 'last_edited_time':
        return new Date(property.last_edited_time);
      case 'title': // Why is title an array ?
        return property.title.map((t: any) => t.plain_text).join(' ');
      case 'rich_text':
        return property.rich_text.map((t: any) => t.plain_text).join(' ');
      case 'files':
        return property.files.map((file: any) => file.name);
      case 'formula':
        if (property.formula.type === 'string') {
          return property.formula.string || undefined;
        } else if (property.formula.type === 'number') {
          return property.formula.number;
        } else if (property.formula.type === 'boolean') {
          return property.formula.boolean;
        } else if (property.formula.type === 'date') {
          return (
            property.formula.date?.start &&
            new Date(property.formula.date.start)
          );
        } else {
          return this.assertUnreachable(property.formula);
        }
      case 'rollup':
        if (property.rollup.type === 'number') {
          return property.rollup.number;
        } else if (property.rollup.type === 'date') {
          return (
            property.rollup.date?.start && new Date(property.rollup.date?.start)
          );
        } else if (property.rollup.type === 'array') {
          return property.rollup.array;
        } else {
          return this.assertUnreachable(property.rollup);
        }
      case 'relation':
        if (property.relation) {
          // @TODO deal with multiple relations ?
          return property.relation.map((rel: any) => rel.id);
        }
        return undefined;
      case 'status':
        return property.status;
    }
    return this.assertUnreachable(property);
  }

  extractValueToString(
    // @ts-ignore
    property: GetPageResponse['properties'][string]
  ): string {
    switch (property.type) {
      case 'checkbox':
        return property.checkbox.toString();
      case 'created_by':
        return this.userToString(property.created_by);
      case 'created_time':
        return new Date(property.created_time).toISOString();
      case 'date':
        return new Date(property.date.start).toISOString();
      case 'email':
        return property.email;
      case 'url':
        return property.url;
      case 'number':
        return property.number;
      case 'phone_number':
        return property.phone_number;
      case 'select':
        if (!property.select) {
          return '';
        }
        return property.select.name;
      case 'multi_select':
        if (!property.multi_select) {
          return '';
        }
        return property.multi_select
          .map((select: any) => `${select.id} ${select.name}`)
          .join(', ');
      case 'people':
        return property.people
          .map((person: any) => this.userToString(person))
          .join(', ');
      case 'last_edited_by':
        return this.userToString(property.last_edited_by);
      case 'last_edited_time':
        return new Date(property.last_edited_time).toISOString();
      case 'title':
        return property.title.map((t: any) => t.plain_text).join(', ');
      case 'rich_text':
        return property.rich_text.map((t: any) => t.plain_text).join(', ');
      case 'files':
        return property.files.map((file: any) => file.name).join(', ');
      case 'formula':
        if (property.formula.type === 'string') {
          return property.formula.string || '???';
        } else if (property.formula.type === 'number') {
          return property.formula.number?.toString() || '???';
        } else if (property.formula.type === 'boolean') {
          return property.formula.boolean?.toString() || '???';
        } else if (property.formula.type === 'date') {
          return (
            (property.formula.date?.start &&
              new Date(property.formula.date.start).toISOString()) ||
            '???'
          );
        } else {
          return this.assertUnreachable(property.formula);
        }
      case 'rollup':
        if (property.rollup.type === 'number') {
          return property.rollup.number?.toString() || '???';
        } else if (property.rollup.type === 'date') {
          return (
            (property.rollup.date?.start &&
              new Date(property.rollup.date?.start).toISOString()) ||
            '???'
          );
        } else if (property.rollup.type === 'array') {
          return JSON.stringify(property.rollup.array);
        } else {
          return this.assertUnreachable(property.rollup);
        }
      case 'relation':
        if (property.relation) {
          console.dir(property.relation);
          return property.relation.join(',');
        }
        return '???';
    }
    return this.assertUnreachable(property);
  }

  propertiesToString() {
    if (!('properties' in this.notionPage)) {
      return {};
    } else {
      let simpleProps = {};
      Object.entries(this.notionPage.properties).forEach(([name, property]) => {
        let propName = slugify(name, {
          lower: true,
          strict: true
        });
        // @ts-ignore
        simpleProps[propName] = this.extractValueToString(property);
      });
      return simpleProps;
    }
  }

  /**
   * Creates a simple dict from all Notion properties of the page using the slug of the property name as key
   * @param ignoreEmptyProperties boolean Whether to ignore properties with empty values
   * @returns {[id: string]: SimpleProperty} a dict with all properties
   */
  simpleProperties(ignoreEmptyProperties = true) {
    if (!('properties' in this.notionPage)) {
      return {};
    }
    let simpleProps = {};
    Object.entries(this.notionPage.properties).forEach(([name, property]) => {
      let propName = slugify(name, {
        lower: true,
        strict: true
      });
      let value: SimplePropertyValueType = this.simpleProperty(property);
      if (!(ignoreEmptyProperties && !value)) {
        // @ts-ignore
        simpleProps[propName] = value;
      }
    });
    return simpleProps;
  }
}
