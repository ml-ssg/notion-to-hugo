import { AsyncMiddleware, HookContext, NextFunction } from '@feathersjs/hooks';
import emojiRegex from 'emoji-regex';
import {
  ListBlockChildrenResponseResult,
  Annotations
} from 'notion-to-md/build/types';
import fs from 'fs';

// These are FeathersJS hooks that are used to transform the data for rendering in the Notion to Markdown process.

/**
 * Logs the output of the hooked original function.
 * @param context FeathersJS hook context
 * @param next Next function
 */
export async function logResult(context: HookContext, next: NextFunction) {
  await next();
  console.log(context.result);
}

/**
 * Saves a JSON for every page, containing the notion-to-md markdown block data structure.
 * @param context FeathersJS Hook context
 * @param next FeathersJS Next function
 */
export async function logMarkdownBlocks(
  context: HookContext,
  next: NextFunction
) {
  await next();
  const targetDir = './build/markdown-blocks';
  fs.mkdirSync(targetDir, { recursive: true });
  fs.writeFile(
    `${targetDir}/${context.arguments[0]}.json`,
    JSON.stringify(context.result, null, 2),
    (err) => {
      if (err) console.log(err);
    }
  );
}

/**
 * Replace Notion links with a shortcode that finds the link in the Hugo pages database and links to it.
 * @param context FeathersJS Hook context
 * @param next Next function
 */
export async function replacenotionLinkWithShortcode(
  context: HookContext,
  next: NextFunction
) {
  // First, convert the notion link to a markdown link
  await next();

  // Then, replace the markdown link with a Hugo shortcode
  let regex = /(\[(.*)\]\((https:\/\/www\.notion\.so)?\/([a-f0-9]{32})\))/gm;
  if (context.result.match(regex)) {
    context.result = context.result.replace(
      regex,
      '[$2]({{< notionIdURL "$4" >}})'
    );
  }
}

/**
 * Replace english quotes with unicode quotes in the markdown output.
 * Failing to do this will cause the markdown to HTML conversion to be broken.
 * @param context
 * @param next
 */
export async function replaceQuotes(context: HookContext, next: NextFunction) {
  await next();
  context.result = context.result.replace(/[“”]/g, '"');
}

/**
 * Replace YouTube links with shortcode in the markdown output of a block conversion
 * This allows the YouTube links to be properly converted to HTML in Hugo, including
 * allowing playing the no-cookie version of the video.
 * @param context Hook context
 * @param next Next function
 */
export async function youtubeShortcode(
  context: HookContext,
  next: NextFunction
) {
  await next();
  context.result = context.result
    .replace(/\[image\]\(https:\/\/youtu\.be\/(.*)\)/gm, '{{< youtube "$1" >}}')
    .replace(
      /\[image\]\(https:\/\/www.youtube.com\/watch\?v\=(.*)\)/gm,
      '{{< youtube "$1" >}}'
    );
}

/**
 * Clean all emojis from the markdown output.
 * @param context Hook context
 * @param next Next function
 */
export async function removeEmojis(context: HookContext, next: NextFunction) {
  await next();
  context.result = context.result.replace(emojiRegex(), '');
}

/**
 * Replace custom callouts with a matching Hugo shortcode. This shortcode needs to be available in the Hugo theme.
 * @TODO This could be generalized to support other custom callouts.
 */
export function customCalloutShortcode(
  shortCodeName: string,
  matchings: any,
  removeEmojis: boolean = true
): AsyncMiddleware {
  return async (context: HookContext, next: NextFunction): Promise<any> => {
    await next();
    const block = context.arguments[0] as ListBlockChildrenResponseResult;
    if (block && 'type' in block && block.type === 'callout') {
      let cleanMarkdown = context.result.replace(/^> /g, ''); // remove quote Markdown character >
      if (removeEmojis) {
        cleanMarkdown = cleanMarkdown.replace(emojiRegex(), '').trim();
      }
      let shortCodeClass: string = '';
      const color = block.callout.color;
      if (Object.keys(matchings).includes(color)) {
        shortCodeClass = matchings[color];
      }
      if (shortCodeClass != '') {
        context.result = `{{% ${shortCodeName} "${shortCodeClass}" %}}\n${cleanMarkdown}\n{{% /${shortCodeName} %}}`;
      }
    }
  };
}

/**
 * Replace custom callouts with a matching Hugo shortcode. This shortcode needs to be available in the Hugo theme.
 * @TODO This could be generalized to support other custom callouts.
 */
export function noticeCustomCallouts(): AsyncMiddleware {
  return customCalloutShortcode('notice', {
    blue_background: 'note',
    yellow_background: 'info',
    green_background: 'tip',
    red_background: 'warning'
  });
}

/**
 * Converts Markdown image tags to a custom Hugo shortcode, which downloads the remote image and embeds it in the Hugo page.
 * This requires the `notionImage` shortcode.
 * @param context Hook context
 * @param next Next function
 */
export const cacheRemoteImagesWithShortcode = async (
  context: HookContext,
  next: NextFunction
) => {
  const block: ListBlockChildrenResponseResult = context.arguments[0];
  await next();
  if ('type' in block && block.type === 'image') {
    // Replace carriage returns and line feeds produced by notion-to-md when converting rich text to plain text in captions.
    const markdownImageTag = context.result.replace(/[\r\n]+/g, '');
    const regex = /!\[(.*)\]\((.*)\)/;
    let match = markdownImageTag.match(regex);
    if (match) {
      const caption = match[1];
      const url = match[2];
      console.log('Image found');
      context.result = `{{% notionImage "${block.id}" "${caption}" "${url}" %}}`;
    } else {
      console.log('Image not found', context.result);
    }
  }
};
