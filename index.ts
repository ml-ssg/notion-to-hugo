import { NotionToHugo } from './src';
import { Page } from './src/types';
import { Client } from '@notionhq/client';
import 'dotenv/config';
import fs from 'fs';
import { hooks, middleware } from '@feathersjs/hooks';
import {
  logMarkdownBlocks,
  replacenotionLinkWithShortcode,
  cacheRemoteImagesWithShortcode,
  replaceQuotes,
  youtubeShortcode,
  noticeCustomCallouts
} from './src/hooks';
import { NotionToMarkdown } from 'notion-to-md';

/* Load environment variables from a .env file or through environment variables:
 * NOTION_TOKEN=<Internal integration token from Notion>
 */
if (
  process.env.NOTION_API_KEY == undefined ||
  process.env.NOTION_CONFIGURATION_DATABASE_ID == undefined
) {
  console.log(
    'No environment variables found: please set NOTION_API_KEY and NOTION_DATABASE_ID in a .env file or as an environment variable.'
  );
  process.exit(1);
}

console.log('🤫 NOTION API KEY: ', process.env.NOTION_API_KEY);
const notion = new Client({ auth: process.env.NOTION_API_KEY });

const configId = process.env.NOTION_CONFIGURATION_DATABASE_ID;
console.log('📚 NOTION CONFIGURATION DATABASE ID: ', configId + '\n');

if (!configId) process.exit(1);

// Setup Notion to Markdown conversion hooks.
hooks(NotionToMarkdown.prototype, {
  blockToMarkdown: middleware([
    replaceQuotes,
    youtubeShortcode,
    noticeCustomCallouts()
  ]).params('block'),
  pageToMarkdown: middleware([logMarkdownBlocks]),
  toMarkdownString: middleware([
    replacenotionLinkWithShortcode,
    cacheRemoteImagesWithShortcode
  ])
});

/* Initialize NotionToHugo with a Notion client */
const n2h = new NotionToHugo({ notionClient: notion });

let validKeys = ['Homepage', 'Database'];
let defaultContentTypes = ['Page', 'Chapter', 'Part', 'Section'];

// The configuration is loaded from a configuration database with lines containing an item type 'Homepage' or 'Database', and a 'Page' column containing a @mention of a target page or database.
n2h
  .databaseToPages(configId)
  .then((db) => {
    if (db !== undefined) {
      db.pages.map((page: Page) => {
        let title = page.frontmatter?.title;
        let targetId: string = '';
        if (!(title && validKeys.indexOf(title) > -1)) {
          console.error(
            'The title of the properties must be one of ' + validKeys.join(', ')
          );
          process.exit(1);
        }

        if (!page.properties || !('page' in page.properties)) {
          console.error(
            "The configuration database must have a 'Page' column pointing to the target content."
          );
          process.exit(1);
        } else {
          if (!('properties' in page.notionPage)) return;
          let targetProp = page.notionPage.properties['Page'];
          if (targetProp.type == 'rich_text') {
            targetProp.rich_text
              .filter((item: any) => item.type == 'mention')
              .forEach((item: any) => {
                if (!targetId) targetId = item.href.replace(/^.*\//, '');
              });
          }

          if (targetId && targetId !== '') {
            // Home pages are written as single pages in the /content/ folder.
            if (title && title == 'Homepage') {
              n2h.retrievePage(targetId).then((page) => {
                if (page) {
                  n2h.writePage({
                    page: page,
                    baseDir: './content/',
                    fileName: '_index.md',
                    writeIfEmpty: true
                  });
                }
              });
            } else if (title && title == 'Database') {
              n2h.databaseToPages(targetId).then((section) => {
                if (section !== undefined) {
                  // console.dir(section.menu, { depth: null });
                  section.pages.map((page: Page) => {
                    let content = './content/';
                    if (
                      page.properties &&
                      page.properties.type &&
                      defaultContentTypes.indexOf(
                        <string>page.properties.type
                      ) == -1
                    )
                      content += <string>page.properties.type + '/';

                    n2h.writePage({
                      page: page,
                      baseDir: content + page.path,
                      writeIfEmpty: true
                    });
                  });
                }
              });
            }
          }
        }
      });
    }
  })
  .catch((error) => {
    console.error('Could not reach the configuration database: ' + error);
  });
