import { Client } from '@notionhq/client';
import { NotionToMarkdown } from 'notion-to-md';
import { ListBlockChildrenResponseResult } from 'notion-to-md/build/types';
import { NotionToHugoOptions, Page } from './types';
import fs from 'fs';
import YAML from 'yaml';
import * as fsPath from 'path';
import slugify from 'slugify';
import emojiRegex from 'emoji-regex';
import { hooks } from '@feathersjs/hooks';

export * from './hooks';
export class NotionToHugo extends NotionToMarkdown {
  private client: Client;

  /**
   * The constructor initializes the Notion client.
   * @param options The Notion client used for queries
   */
  constructor(options: NotionToHugoOptions) {
    super(options);
    this.client = options.notionClient;
  }

  async retrievePage(pageId: string): Promise<Page | undefined> {
    return this.client.pages.retrieve({ page_id: pageId }).then((page) => {
      return new Page(page);
    });
  }

  /**
   * Retrieves a Notion database as a hierarchical collection of Pages}. The relationship between pages is made using a *relation* column.
   * @param databaseId The Notion Id of the database
   * @param parentPropertyName The name of the database column used to create relationships between pages. This HAS to be of Notion type 'relation'
   * @returns {menu, pages:Page[]} An object containing the menu and Pages for each named page (which has a title)
   */
  databaseToPages(
    databaseId: string,
    fetchPages: boolean = false,
    parentPropertyName: string = 'Parent'
  ) {
    /*
     * Build a menu tree for a collection of pages from the pages' parent information.
     * It enriches the page objects with 'children' properties, and a /-separated path for each node.
     */
    function getMenuTree(pages: Page[] = []) {
      const idMapping = pages.reduce((acc: any, page, i) => {
        acc[page.id] = i;
        return acc;
      }, {});

      let root = {};
      pages.forEach((page) => {
        if (!('properties' in page) || page.properties == undefined) return '';
        let slug = slugify(parentPropertyName, { lower: true, strict: true });
        let parentProp = page.properties[slug];
        if (parentProp && Array.isArray(parentProp)) {
          if (parentProp.length == 0) {
            //@ts-ignore
            root.children = [...(root.children || []), page];
            return;
          } else {
            // Handle the root element
            let parentId = parentProp[0].replace(/-/g, '');

            // Use our mapping to locate the parent element in our data array
            const parentEl = pages[idMapping[parentId]];
            // Add our current el to its parent's `children` array
            parentEl.children = [...(parentEl.children || []), page];
          }
        }
      });
      return root;
    }

    // Adds a path information to the pages, to save them in the proper subfolders to make the menu work automagically.
    function addPathToPagesInTree(leafOrNode: any, start: string = '') {
      if ('children' in leafOrNode) {
        leafOrNode.children?.forEach((child: Page) => {
          child.path = start;
          let subPath = child.path + '/' + child.frontmatter?.slug;
          addPathToPagesInTree(child, subPath);
        });
      }
    }

    return this.client.databases
      .query({
        database_id: databaseId,
        filter: {
          or: [
            {
              property: 'title',
              rich_text: {
                is_not_empty: true
              }
            }
          ]
        }
      })
      .then((query) => {
        // Convert Notion pages to Hugo pages for convenience.
        let pages = query.results.map((page) => {
          let hPage: Page = new Page(page);
          return hPage;
        });

        let menu = getMenuTree(pages);
        addPathToPagesInTree(menu);

        return {
          pages: pages,
          menu: menu
        };
      })
      .catch((error) => {
        console.error(error);
        return undefined;
      });
  }

  writePage({
    page,
    baseDir = './content',
    fileName = undefined,
    frontMatter = true,
    writeIfEmpty = false,
    subdir = true
  }: {
    page: Page;
    baseDir?: string;
    fileName?: string;
    frontMatter?: boolean;
    writeIfEmpty?: boolean;
    subdir?: boolean;
  }) {
    return new Promise((resolve) => {
      this.pageToMarkdown(page.id)
        .then((mdblocks) => this.toMarkdownString(mdblocks))
        .then((mdString) => {
          let message = '';
          let empty = mdString.length == 0;
          if (frontMatter) {
            const fm = YAML.stringify(page.frontmatter);
            mdString = `---\n${fm}---${mdString}`;
          }
          try {
            // We only write non-empty files.
            if (!(!writeIfEmpty && empty)) {
              let dest: string;
              var outDir: string = baseDir;
              let extension: string = 'md';
              if (page.language) {
                extension = `${page.language}.${extension}`;
              }
              if (!fileName && subdir) {
                // By default, we write the file in an index file in a subdirectory named after the slug.
                outDir = fsPath.join(baseDir, page.frontmatter?.slug || '');
                dest = fsPath.join(outDir, `index.${extension}`);
              } else {
                if (fileName) {
                  // If there is a filename, we use it to create the file.
                  dest = fsPath.join(outDir, fileName);
                } else {
                  // If there is no filename, we use the page's slug as file name.
                  dest = fsPath.join(
                    outDir,
                    page.frontmatter?.slug + '.' + extension
                  );
                  console.log(dest);
                }
              }

              if (!fs.existsSync(outDir)) {
                fs.mkdirSync(outDir, { recursive: true });
              }

              fs.writeFile(dest, mdString, (err) => {
                console.log('✅ ' + page.frontmatter?.title);
                if (err) {
                  console.error(page.frontmatter?.title, err);
                }
              });
            } else {
              console.log('🗌 ' + page.frontmatter?.title + ' (empty page)');
            }
            return message;
          } catch (error) {
            console.error('Error with file ' + page.frontmatter?.title, error);
          }
        });
    });
  }
}
